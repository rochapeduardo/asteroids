﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoAsteroids.View
{
    class GameObject
    {
        Texture2D texture;
        public Texture2D Texture { get { return texture; } set { texture = value; center = new Vector2(texture.Width / 2, texture.Height / 2); } }
        float rotation;
        protected Vector2 forward { get; private set; }
        public float Rotation
        {
            get
            {
                return rotation;
            }

            set
            {
                forward = new Vector2( (float) Math.Sin(Convert.ToDouble(value)), (float) -Math.Cos(Convert.ToDouble(value)));
                rotation = value;
            }
        }
        public Vector2 position = Vector2.Zero;
        Vector2 center = Vector2.Zero;
        public String debugTextureName = String.Empty;
        public Texture2D debugTexture;
        public Color color = Color.White;

        public GameObject()
        {
            Rotation = 0;
        }

        #region XNA framework methods
        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position - center, null, color, rotation, center, 1, SpriteEffects.None, 0);
        }
        #endregion
    }
}
