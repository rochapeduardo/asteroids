﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoAsteroids.View
{
    class Asteroid : GameObject
    {
        public float rotation = 0;
        float maxSpeed = 2;
        Random random = new Random();
        int direction = 0;
        double speed = 0;
        int randomdir;
        public int size = 3;

        public void Start(double Speedrand, int Directionrand, int randomDir)
        {

            direction = Directionrand;

            speed = Speedrand;

            randomdir = randomDir;



        }
        

        public void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            #region Mudar pos na tela
            if (this.position.X < -250) {

                this.position.X = 890;
            }

            if (this.position.Y < -250) {


                this.position.Y = 790;
            }
            if (this.position.Y > 800)
            {
                this.position.Y = -240;
            }

            if (this.position.X > 900)
            {
                this.position.X = -240;
            }
            #endregion



            if (direction == 1) {

                this.position.Y -= (float)speed;
                this.position.X += randomdir;
            }
            if (direction == 2)
            {
                this.position.Y += (float)speed;

                this.position.X += randomdir;

            }
            if (direction == 3)
            {
                this.position.X += (float)speed;

                this.position.Y += randomdir;

            }
            if (direction == 4)
            {

                this.position.X -= (float)speed;

                this.position.Y += randomdir;
            }


            base.Update(gameTime);
        }
    }
}
