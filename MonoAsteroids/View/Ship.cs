﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MonoAsteroids.View
{
    class Ship : GameObject
    {
        float maxSpeed = 5;
        float accelaration = 0.2f;
        float speed = 0;
        static KeyboardState currentState;
        static KeyboardState previousState;
        public Vector2 direction;
        GamePadState old;
        float rotationSpeed = .1f;
        Keys theKey;

        public override void Update(GameTime gameTime)
        {
            #region Testing
            if (this.position.X < -50)
            {

                this.position.X = 840;
            }

            if (this.position.Y < -50)
            {
                this.position.Y = 640;
            }
            if (this.position.Y > 650)
            {
                this.position.Y = -40;
            }

            if (this.position.X > 850)
            {
                this.position.X = -40;
            }
            #endregion

            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);

            if (gamePadState.Buttons.X == ButtonState.Pressed && old.Buttons.X == ButtonState.Released)
            {
                // Shoot
            }
            old = gamePadState;


            if (gamePadState.DPad.Left == ButtonState.Pressed)
            {
                if (Rotation - rotationSpeed < -MathHelper.TwoPi)
                {
                    Rotation = MathHelper.TwoPi;
                }
                else
                {
                    Rotation -= rotationSpeed;
                }
            }


            if (gamePadState.DPad.Right == ButtonState.Pressed)
            {
                if (Rotation + rotationSpeed > MathHelper.TwoPi)
                {
                    Rotation = -MathHelper.TwoPi;
                }
                else
                {
                    Rotation += rotationSpeed;
                }
            }

            if (gamePadState.DPad.Up == ButtonState.Pressed)
            {
                speed = MathHelper.Clamp(speed + accelaration * (float)gameTime.ElapsedGameTime.TotalSeconds, 0, maxSpeed);
                direction = Vector2.Lerp(direction, forward, speed / maxSpeed);
            }

            if (gamePadState.DPad.Up == ButtonState.Released)
            {
                speed = MathHelper.Clamp(speed - accelaration * (float)gameTime.ElapsedGameTime.TotalSeconds, 0, maxSpeed);
            }

            //#region Keyboard Inputs
            //KeyboardState state = Keyboard.GetState();

            //if (state.IsKeyDown(Keys.Left))
            //{
            //    if (Rotation - rotationSpeed < -MathHelper.TwoPi)
            //    {
            //        Rotation = MathHelper.TwoPi;
            //    }
            //    else
            //    {
            //        Rotation -= rotationSpeed;
            //    }
            //}

            //if (state.IsKeyDown(Keys.Right))
            //{
            //    if (Rotation + rotationSpeed > MathHelper.TwoPi)
            //    {
            //        Rotation = -MathHelper.TwoPi;
            //    }
            //    else
            //    {
            //        Rotation += rotationSpeed;
            //    }
            //}

            //if (state.IsKeyDown(Keys.Up))
            //{
            //    speed = MathHelper.Clamp(speed + accelaration * (float) gameTime.ElapsedGameTime.TotalSeconds, 0, maxSpeed);
            //    direction = Vector2.Lerp(direction, forward, speed/maxSpeed);
            //}

            //if (state.IsKeyUp(Keys.Up))
            //{
            //    speed = MathHelper.Clamp(speed - accelaration * (float)gameTime.ElapsedGameTime.TotalSeconds, 0, maxSpeed);
            //}
            //#endregion

            position += (direction * speed);

            base.Update(gameTime);
        }
    }
}