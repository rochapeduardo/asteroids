﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MonoAsteroids.View
{
    class Missile : GameObject
    {
        float speed = 10;
        public Vector2 dir;

        public void Start(Vector2 direction, float shipRot)
        {
            dir = direction;
            this.Rotation = shipRot;

        }
        public void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {

            this.position += (dir * speed);

            #region Mudar pos na tela
            if (this.position.X < -250)
            {

                this.position.X = 890;
            }

            if (this.position.Y < -250)
            {


                this.position.Y = 790;
            }
            if (this.position.Y > 800)
            {
                this.position.Y = -240;
            }

            if (this.position.X > 900)
            {
                this.position.X = -240;
            }
            #endregion

            base.Update(gameTime);
        }
    }
}