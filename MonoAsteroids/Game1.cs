﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoAsteroids.View;
using System;

namespace MonoAsteroids
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private GamePadState old;

        readonly int screenWidth = 800;
        readonly int screenHeight = 600;

        Random random = new Random();
        int asteroidCount = 4;
        int maxAsteroidCount;


        int maxMissile = 6;
        int curMissileCount = 0;

        #region ENTIDADES
        Ship ship;
        Missile[] missile;
        Asteroid[] asteroids;
        #endregion

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = screenWidth;
            graphics.PreferredBackBufferHeight = screenHeight;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            ship = new Ship { Texture = Content.Load<Texture2D>("Sprites/Ship") };
            ship.position = new Vector2(screenWidth / 2, screenHeight / 2);
            //ship.Position = Vector2.Zero;

            #region Missile

            missile = new Missile[maxMissile];


            for (int i = 0; i < maxMissile; i++)
            {
                this.missile[i] = new Missile { Texture = Content.Load<Texture2D>("Sprites/Missile") };

            }

            #endregion


            #region ASTEROID

            maxAsteroidCount = asteroidCount * 4;


            asteroids = new Asteroid[maxAsteroidCount];


            for (int i = 0; i < maxAsteroidCount; i++)
            {
                if (i < asteroidCount)
                {
                    this.asteroids[i] = new Asteroid { Texture = Content.Load<Texture2D>("Sprites/Asteroid_1") };
                }
                if (i >= asteroidCount && i < asteroidCount * 2)
                {
                    this.asteroids[i] = new Asteroid { Texture = Content.Load<Texture2D>("Sprites/Asteroid_2") };

                }
                if (i >= asteroidCount * 2 && i < asteroidCount * 4)
                {
                    this.asteroids[i] = new Asteroid { Texture = Content.Load<Texture2D>("Sprites/Asteroid_3") };
                }
            }



            random = new Random();

            for (int i = 0; i < maxAsteroidCount; i++)
            {

                asteroids[i].position = new Vector2(random.Next(0, screenWidth), random.Next(0, screenHeight));
                asteroids[i].Start(random.NextDouble() * 2, random.Next(1, 4), random.Next(-3, 3));

            }


            #endregion
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {


            for (int i = 0; i < asteroidCount; i++)
            {
                asteroids[i].Update(gameTime);
            }

            //CHECA COLISAO ENTRE A NAVE E OS ASTEROIDS. 
            //IDEALMENTE SERIA UMA LISTA PARA TODOS OS GAME OBJECTS.
            foreach(Asteroid a in asteroids)
            {
                if (Collision.DistanceCollision.CheckCollision(ship, a))
                {
                    Console.WriteLine("Colidiu");
                    ship.color = Color.Red;
                }
                else
                {
                    ship.color = Color.Green;
                }
            }
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);

            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            if (gamePadState.Buttons.X == ButtonState.Pressed && old.Buttons.X == ButtonState.Released)
            {
                // Shoot

                if (curMissileCount < maxMissile)
                {
                    curMissileCount += 1;
                    missile[curMissileCount].position = new Vector2(ship.position.X, ship.position.Y);
                    missile[curMissileCount].Start(ship.direction, ship.Rotation);

                }
            }
            old = gamePadState;

            for (int i = 0; i < curMissileCount; i++)
            {
                missile[i].Update(gameTime);
            }
            ship.Update(gameTime);

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            // TODO: Add your drawing code here
            ship.Draw(spriteBatch);

            for (int i = 0; i < asteroidCount; i++)
            {
                asteroids[i].Draw(spriteBatch);
            }

            for (int i = 0; i < curMissileCount; i++)
            {
                missile[i].Draw(spriteBatch);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
