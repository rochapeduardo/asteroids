﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoAsteroids.View;
using System;

namespace MonoAsteroids.Collision
{
    static class DistanceCollision
    { 
        static public bool CheckCollision(GameObject a, GameObject b)
        {
            double distance;

            //distance = Math.Sqrt(Math.Pow(a.position.X - b.position.X, 2) 
            //                  + Math.Pow(a.position.Y - b.position.Y, 2));

            distance = Math.Abs(a.position.X - b.position.X) + Math.Abs(a.position.Y - b.position.Y);

            if (distance > a.Texture.Width / 2 + b.Texture.Width / 2)
                return false;
            else
                return true;
        }
    }
}
